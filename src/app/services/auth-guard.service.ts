import {Injectable} from '@angular/core';
import {NavController} from '@ionic/angular';

@Injectable({
    providedIn: 'root'
})
export class AuthGuardService {

    private logged: boolean;
    private username: string = 'user';

    constructor() {
      this.logged = false;
    }

    public canActivate(): boolean {
        return this.logged;
    }

    public login(username: string) {
        if (username !== '') {
            this.logged = true;
            this.username = username;
        }
    }

    public getUser(): string {
        return this.username;
    }

    public logout() {
    }
}
